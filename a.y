%{

      #include "symbtab.c"
      #include "quadMat.c"
      #include "pile.c"

      int type = -1;
      int taille = -1;
      char isConst[4] = "non";

       int linenum ;
       int qc ;

      int compatibilite = -1;
      int type_const = 0;

      //pour operer sur les bibleotheque
      int BIB_ARITHM_count = 0;
      int BIB_TAB_count = 0;
      int BIB_InOut_count = 0;

      char operator;
      float fv;

      int debINST = 0;
      int finINST = 0;
      int FIN = 0 ;

      char OP_CON[3];
      int i = 0;
      char c[5];
      char v[8];

      char string[10];



%}

%union{
        int entier;
        float reel;
        char *str;
}

%token MC_LANG <str>MC_IDF MC_FINDEC  MC_VAR MC_DEBUT MC_FIN

%token MC_SEMI MC_COMMA  MC_DP MC_AFFECT MC_DECLARE

%token <entier>MC_INT <reel>MC_FLOAT <str>MC_STRING

%token <str>MC_TAB  <str>MC_CONST

%token <str>MC_BIB_TAB <str>MC_BIB_ARITHM <str>MC_BIB_INOUT

%token L_PAREN R_PAREN L_BRACK R_BRACK L_BRACE R_BRACE

%token MC_ADD MC_SUB MC_MUL MC_DIV MC_MOD

%token MC_STRICT_SUP MC_STRICT_INF MC_SUP_EQUAL MC_INF_EQUAL
MC_NOT_EQUAL MC_EQUAL

%token <entier>INT_CONST <reel>FLOAT_CONST <str>STRING_CONST

%token MC_EXECUTE MC_IF

%token MC_WHILE MC_FAIRE MC_FAIT

%token MC_INPUT MC_OUTPUT

%%
S:MC_LANG MC_IDF BIB MC_VAR Declaration MC_FINDEC MC_DEBUT Instruction MC_FIN {
      printf("\nProgramme correct:D\n");
      YYACCEPT;
      }
;

Declaration: Variable Declaration
           | Constante Declaration
           | Tableaux Declaration
           |
;


Variable:Type MC_DP{strcpy(isConst,"non");} list_idf MC_SEMI
;


Constante:MC_CONST {strcpy(isConst,"oui");} Type MC_DP list_idf MC_SEMI
;

Tableaux:MC_TAB MC_DP Type MC_IDF R_BRACK INT_CONST L_BRACK MC_SEMI  {
            //verifier la taille du tableau
            if($6 <= 0 ){
                  errorSemantic(linenum,"taille du tableau\n");
            }else{
                  if(BIB_TAB_count != 1){
                        missingLibrary(linenum,"BIB_TAB");
                  }else{
                        insererType($4,type);
                        setSize($4,$6);
                        compatibilite = getType($4);
                        //index = getSize($4);
                   }
            }
              }
;

list_idf: MC_IDF {
      if(DoubleDeclaration($1) == 1){

        insererType($1,type);
        setConstant($1,isConst);
        compatibilite = getType($1);

      }else{
            errorSemantic(linenum,"IDF DOUBLE DECLARE\n");
                       // exit(EXIT_FAILURE);

      }}
        |list_idf MC_COMMA MC_IDF {
                  //check for double declaration
                  if(DoubleDeclaration($3) == 1) {

                      insererType($3,type);
                      setConstant($3,isConst);
                      compatibilite = getType($3);

                  }else{
            errorSemantic(linenum,"IDF DOUBLE DECLARED\n");
                        //on declanche une erreur
                  }
            }
;

Type:MC_FLOAT     { type = 1; }
    |MC_INT       { type = 0; }
    |MC_STRING    { type = 2; }
;



BIB:MC_BIB_TAB BIB { BIB_TAB_count ++;
      if(BIB_TAB_count>1) printf("Warning : %s declaree plusiers fois \n",$1);
}
   |MC_BIB_ARITHM BIB { BIB_ARITHM_count ++; if(BIB_TAB_count>1) printf("Warning : %s declaree plusiers fois \n",$1);}
   |MC_BIB_INOUT BIB  { BIB_InOut_count ++; if(BIB_TAB_count>1) printf("Warning : %s declaree plusiers fois \n",$1);}
   |
;


Instruction:Affectation Instruction {/*affect match, quad affect*/}
           |IF Instruction          {/*affect match, quad if*/}
           |While Instruction       {/*affect match, quad while*/}
           |Input Instruction       {/*affect match, quad input*/}
           |Output Instruction      {/*affect match, quad output*/}
           |
;


Affectation:MC_IDF{compatibilite = getType($1);} MC_AFFECT Expression MC_SEMI
           |MC_IDF{compatibilite = getType($1);} R_BRACK INT_CONST L_BRACK MC_AFFECT Expression MC_SEMI {
                  if(indexOutOfBounds($1,$4) == 1){
                        errorSemantic(linenum,"INDEX out OF Bounds\n");
                  }
           }
;


Expression:MC_IDF {
      if((NotDeclared($1) ==1 )){
              errorSemantic(linenum,"NOT DECLARED");
      }else{
      //verifier compatibilite de l'affectation
       if(affect_compatibl(compatibilite,getType($1)) == 0){

                  errorSemantic(linenum,"INCOMPATIBILITE\n");

                  }
            }
}
          |Value {
                //ici on initialise une variable ====> 1
               //  strcpy(val,$1);
          }
          |Expression OP Value {

                  if(divByZero(operator,fv) == 1 )
                        errorSemantic(linenum,"div by 0\n");
                  if(operator == '/' && compatibilite == 0)
                        errorSemantic(linenum,"incompatibilite de divion\n");
                }
          |Expression OP MC_IDF{
            if((NotDeclared($3) == 1 )||(BIB_ARITHM_count == 0)){

                  errorSemantic(linenum,"NOT DECLARED OR BIB_ARITHM must be declared\n");

            }else{
             //verifier compatibilite de l'affectation
            if(affect_compatibl(compatibilite,getType($3)) == 0){
             errorSemantic(linenum,"INCOMPATIBILITE\n");

            }
            }
}
            |MC_IDF R_BRACK INT_CONST L_BRACK {
/* verifier type , bib , et indexOutOfBounds , nonDeclaration */
 if((NotDeclared($1) == 1 )){
      errorSemantic(linenum,"NOT DECLARED");

}else{
      if((affect_compatibl(compatibilite,getType($1)) == 0)){

           errorSemantic(linenum,"INCOMPATIBILITE\n");


       }else{
             //verifier compatibilite de l'affectation ET bibleo
         if((indexOutOfBounds($1,$3) == 1)||(BIB_TAB_count== 0)){

           errorSemantic(linenum,"INDEX OUT OF BOUNDS \n");
                  }
            }

            }
}
            |Expression OP MC_IDF R_BRACK INT_CONST L_BRACK {
/* verifier type , bib , et indexOutOfBounds , nonDeclaration */
 if((NotDeclared($3) == 1 )){
      errorSemantic(linenum,"NOT DECLARED");
}else{
      if((affect_compatibl(compatibilite,getType($3)) == 0)){

           errorSemantic(linenum,"INCOMPATIBILITE\n");

       }else{
                  //verifier compatibilite de l'affectation ET bibleo
         if((indexOutOfBounds($3,$5) == 1)||(BIB_TAB_count== 0)){

           errorSemantic(linenum,"INDEX OUT OF BOUNDS \n");

                  }
            }

            }
}
;
OP:MC_ADD   {operator = ' ';}
  |MC_SUB   {operator = ' ';}
  |MC_MUL   {operator = ' ';}
  |MC_DIV   {operator = '/';}
  |MC_MOD   {operator = ' ';}
;


Value:INT_CONST   {
      type_const = 0;
      if((affect_compatibl(compatibilite,type_const) == 0)){
           errorSemantic(linenum,"INCOMPATIBILITE CONST_VALUE\n");

      }
      fv = $1;
}
     |FLOAT_CONST  {
           type_const = 1;
      if((affect_compatibl(compatibilite,type_const) == 0)){
           errorSemantic(linenum,"INCOMPATIBILITE  CONST_VALUE\n");
      }
      fv = $1;
}
     |STRING_CONST      {
           type_const = 2;
      if((affect_compatibl(compatibilite,type_const) == 0)){
           errorSemantic(linenum,"INCOMPATIBILITE CONST_VALUE\n");
      }
}
;

OP_COND:MC_SUP_EQUAL    {  strcpy(OP_CON,">=");      }
       |MC_INF_EQUAL    {  strcpy(OP_CON,"<=");      }
       |MC_STRICT_SUP   {  strcpy(OP_CON,">");      }
       |MC_STRICT_INF   {  strcpy(OP_CON,"<");      }
       |MC_EQUAL        {  strcpy(OP_CON,"==");      }
       |MC_NOT_EQUAL    {  strcpy(OP_CON,"!=");      }
;

/*Condition:Expression OP_COND Expression
;*/
Condition:MC_IDF OP_COND MC_IDF {

      strcpy(v,"t");
      sprintf(c,"%d",i);
      strcat(v,c);
      i++;

      insererQUADR(OP_CON,$1,$3,v);
}
;

IF:A R_PAREN
;

A:B MC_IF L_PAREN Condition {//R3

      FIN = qc;
      insererQUADR("BNZ",string,v,"");
      sprintf(string,"%d",FIN);
      ajour_quad(finINST,1,string);
};

B:C Instruction {//R2
     finINST = qc;
     insererQUADR("BR","","","");
     strcpy(string,"");
     sprintf(string,"%d",finINST);
     ajour_quad(debINST,1,string);
};
C: MC_EXECUTE {//R1
     debINST = qc;
     insererQUADR("BR","","","");
     strcpy(string,"");
     sprintf(string,"%d",debINST);
};
While:MC_WHILE L_PAREN Condition R_PAREN MC_FAIRE Instruction MC_FAIT
;

Input:MC_INPUT L_PAREN ValidInput R_PAREN MC_SEMI {
      if(BIB_InOut_count ==0 ){
            missingLibrary(linenum,"#InOut must be declared\n");
      }
}
;

ValidInput: STRING_CONST
          |STRING_CONST MC_COMMA ValidInput
;

Output:MC_OUTPUT L_PAREN valid_output R_PAREN MC_SEMI {
      if(BIB_InOut_count ==0 ){
            missingLibrary(linenum,"#InOut must be declared\n");
      }
}
;

valid_output:MC_IDF {
      if((NotDeclared($1) == 1 )){
            errorSemantic(linenum,"NOT DECLARED_IDF\n");
      }
}
            | Tableaux
            |MC_IDF MC_COMMA valid_output  {
                  if((NotDeclared($1) == 1 )){
                  errorSemantic(linenum,"NOT DECLARED_IDF\n");
                  //exit(EXIT_FAILURE);
            }
}
            |Tableaux MC_COMMA valid_output
            |
;
Tableaux:MC_IDF R_BRACK INT_CONST L_BRACK {
            //verifier compatibilite de l'affectation ET bibleo
         if((NotDeclared($1) == 1 )){
           errorSemantic(linenum,"Tableau not declared ERROR\n");
                  }
          if((BIB_TAB_count== 0)){
            missingLibrary(linenum,"'#TAB must be declared\n");
          }

          if(indexOutOfBounds($1,$3) == 1){
            errorSemantic(linenum,"indexOutOfBounds\n");
          }
}
;

%%
 int main(int argc,char **argv){

      init();           //elle initialise ma table de ymbole
      initialize();     //elle initialiser ma matrice quad

      yyparse();

      afficherTS();
      printQuad(quadMatrix);

}
yywrap(){
}
