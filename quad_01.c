#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct quadNode quadNode;
struct quadNode{

    char oper[10];
    char op1[10];
    char op2[10];
    char res[10];
    quadNode *next;
};

typedef struct QUAD QUAD;
struct QUAD{
    quadNode *head;
};


int qc = 0 ;

QUAD quadMatrix;

void initialize(){

    quadMatrix.head = malloc(sizeof(quadMatrix.head));

    quadMatrix.head = NULL;
    //return quadMatrix;
}

quadNode *creatNode(char opr[],char op1[],char op2[],char res[]){

    quadNode *new = malloc(sizeof(*new));

    strcpy(new->oper  ,   opr);
    strcpy(new->op1   ,   op1);
    strcpy(new->op2   ,   op2);
    strcpy(new->res   ,   res);
    new->next = NULL;

    return new;
}


void insererQUADR(char opr[],char op1[],char op2[],char res[]){

    quadNode *temp = quadMatrix.head;

    quadNode *new = creatNode(opr,op1,op2,res);

    if(new == NULL )
        exit(EXIT_FAILURE);

    //insertion are in FIFO
    if(temp == NULL){
        temp = new;
    }else {
        while (temp->next != NULL) {
            temp = temp->next;
        }
        temp->next = new;
    }
    qc++;
}

void delete(){
    if(quadMatrix.head == NULL)
        exit(EXIT_FAILURE);
    if(quadMatrix.head != NULL){

        quadNode *toBeDeleted = quadMatrix.head;
        quadMatrix.head = quadMatrix.head->next;
        free(toBeDeleted);
    }
}

void printQuad(){

    printf("*********************LesQuadruplets***********************\n");

    if(quadMatrix.head == NULL ){
        exit(EXIT_FAILURE);
    }

    quadNode *actual = quadMatrix.head;

    int i = 0;
    while(actual != NULL)
    {
        //printf("%a ---- %a \n",quadMatrix,quadMatrix->head);
        printf("\n %d - ( %s  ,  %s  ,  %s  ,  %s )",i,actual->oper,actual->op1,actual->op2,actual->res);
        printf("\n---------------------------------------------------\n");

        actual = actual->next;
        i++;
    }
}


int main(int argc, char const *argv[])
{
    initialize();

    /*quadMatrix.head = creatNode("/","A","B","Temp1");
    quadMatrix.head->next = creatNode("+","A","B","Temp2");
    quadMatrix.head->next->next = creatNode("-","Temp1","temp2","Temp2");
    quadMatrix.head->next->next->next = creatNode("+","4","B","Temp3");
*/
    insererQUADR("/","A","B","Temp1");
/*quadMatrix.head->next = creatNode("+","A","B","Temp2");*/
    printQuad();
    return 0;
}
